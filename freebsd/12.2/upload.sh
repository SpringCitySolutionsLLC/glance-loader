#!/bin/bash
#
# glance-loader/freebsd/12.2/upload.sh
#

source /etc/kolla/admin-openrc.sh

openstack image create \
  --container-format bare \
  --public \
  --property os_type=linux \
  --property os_distro=freebsd \
  --property os-version=12.2 \
  --property os_admin_user=freebsd \
  --min-ram 1024 \
  --min-disk 2 \
  --disk-format qcow2 \
  --file freebsd-12.2.qcow2 \
  freebsd-12.2

exit 0
