#!/bin/bash
#
# glance-loader/freebsd/13.0-zfs/upload.sh
#

source /etc/kolla/admin-openrc.sh

openstack image create \
  --container-format bare \
  --public \
  --property os_type=linux \
  --property os_distro=freebsd \
  --property os-version=13.0 \
  --property os_admin_user=freebsd \
  --min-ram 1024 \
  --min-disk 2 \
  --disk-format qcow2 \
  --file freebsd-13.0-zfs.qcow2 \
  freebsd-13.0-zfs

exit 0
