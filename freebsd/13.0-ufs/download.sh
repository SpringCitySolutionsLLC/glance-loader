#!/bin/bash
#
# glance-loader/freebsd/13.0-ufs/download.sh
#

# OK here is the story:
# The official freebsd cloud images from:
# https://download.freebsd.org/ftp/releases/VM-IMAGES/13.0-RELEASE/amd64/Latest/
# are good images that technically work, sort of.  They have some problems.
# The root password is the enter key.
# The users (such as freebsd) are not created, so can't inject ssh keys, so can't log in over the network.
# SSHD is not started, so even if you had keys injected you're not logging in.
# The serial console is not enabled (which openstack can use for logs)
# It doesn't seem to support some cloud-init features.

# On the other hand, this URL at openstack:
# https://docs.openstack.org/image-guide/obtain-images.html
# Officially suggests the unofficial images at
# https://bsd-cloud-image.org/
# And you can replicate those binaries by running your own copy of:
# https://github.com/virt-lightning/freebsd-cloud-images/blob/master/build.sh
# running it yourself is probably better than trusting someone elses md5sums.

# Ideally I'd run images directly from FreeBSD, but 
# given the option of images that are official, 
# or of images that actually work...

# Also the qcow size for the official image is 3.4 gigs but 
# the bsd-cloud-image images are only 1508 megs.

# Note the 13.0-UFS auto-expands the root partition, 13.0-ZFS does NOT, but see resize.txt

wget -c https://object-storage.public.mtl1.vexxhost.net/swift/v1/1dbafeefbd4f4c80864414a441e72dd2/bsd-cloud-image.org/images/freebsd/13.0/freebsd-13.0-ufs.qcow2

echo sha256sum freebsd-13.0-ufs.qcow2 result should be:
echo 64d1d9a3aa4b0cf118c7338bf57ec62005a436a23d3f82499a786690275ee5ee  freebsd-13.0-ufs.qcow2
echo
echo sha256sum freebsd-13.0-ufs.qcow2 result is:
sha256sum freebsd-13.0-ufs.qcow2

exit 0
