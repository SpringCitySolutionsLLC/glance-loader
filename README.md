README.md

# What is this?

These are PUBLIC scripts to repeatably reliably consistently load OS images into an OpenStack Glance server.

This provides consistent configuration across all my systems, at extreme speed, for low effort.

# Commentary

This repo makes the most sense in the context of being "upstream" of the following repos:

https://gitlab.com/SpringCitySolutionsLLC/openstack-scripts

https://gitlab.com/SpringCitySolutionsLLC/ansible

https://gitlab.com/SpringCitySolutionsLLC/dhcp

## download.sh

Downloads the operating system image.  I use options to continue interrupted downloads.

Display on the screen how you could find the upstream file checksum.

Display on the screen the file checksum, as I was informed as of the last time I edited the script.

Calculate and display the checksum of whatever was downloaded, for your comparison.

## upload.sh

Anyone can upload a Glance image with no tags and no metadata and an unclear or imprecise image name.

I put work into providing the best possible metadata and tagging, so you don't have to.

## clean.sh

Wipes downloaded image file to save disk space.

Probably a waste of time in the current year.

## Standards

1) I use plain language shell scripts; you should "Trust but Verify" and read them before running them.

2) I do not "git commit" until it boots, I can log in, and I can ping my router.

3) I try my best to only download direct from upstream with exceptions only for major functional problems.

4) I try to provide the "best" image WRT OpenStack support.  If I picked the "wrong" image, please let me know!

## How to Help

If you have a request or a bug, please submit issues in GitLab or even create a pull request.

