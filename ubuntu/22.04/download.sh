#!/bin/bash
#
# glance-loader/ubuntu/22.04/download.sh
#

#wget -c https://cloud-images.ubuntu.com/jammy/20220810/jammy-server-cloudimg-amd64.img

echo md5sum value comes from
echo http://cloud-images.ubuntu.com/jammy/20220810/MD5SUMS
echo
echo md5sum jammy-server-cloudimg-amd64.img result should be:
echo dee31fb1d8f5400af8f8903f0d483e7c jammy-server-cloudimg-amd64.img
echo
echo md5sum jammy-server-cloudimg-amd64.img is:
md5sum jammy-server-cloudimg-amd64.img

exit 0
