#!/bin/bash
#
# glance-loader/ubuntu/22.04/upload.sh
#

source /etc/kolla/admin-openrc.sh

openstack image create \
  --container-format bare \
  --public \
  --property os_type=linux \
  --property os_distro=ubuntu \
  --property os-version=22.04 \
  --property os_admin_user=ubuntu \
  --min-ram 256 \
  --min-disk 1 \
  --disk-format qcow2 \
  --file jammy-server-cloudimg-amd64.img \
  ubuntu-22.04

exit 0
