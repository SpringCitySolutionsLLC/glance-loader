#!/bin/bash
#
# glance-loader/ubuntu/20.04/download.sh
#

# It seems to be widely believed on the internet that if you 
# download a -kvm image, console will not work in openstack.
# This seems to be correct?
# wget -c https://cloud-images.ubuntu.com/focal/20220810/focal-server-cloudimg-amd64-disk-kvm.img

wget -c https://cloud-images.ubuntu.com/focal/20230215/focal-server-cloudimg-amd64.img

echo md5sum value comes from
echo http://cloud-images.ubuntu.com/focal/20230215/MD5SUMS
echo
echo md5sum focal-server-cloudimg-amd64.img result should be:
echo 63dad2e3091bfd1c198e8bc38f318018 focal-server-cloudimg-amd64.img
echo
echo md5sum focal-server-cloudimg-amd64.img is:
md5sum focal-server-cloudimg-amd64.img

exit 0
