#!/bin/bash
#
# glance-loader/cirros/0.4.0/download.sh
#

wget -c http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img

echo md5sum value comes from
echo http://download.cirros-cloud.net/0.4.0/MD5SUMS 
echo
echo md5sum cirros-0.4.0-x86_64-disk.img result should be:
echo 443b7623e27ecf03dc9e01ee93f67afe  cirros-0.4.0-x86_64-disk.img
echo
echo md5sum cirros-0.4.0-x86_64-disk.img is:
md5sum cirros-0.4.0-x86_64-disk.img

exit 0
