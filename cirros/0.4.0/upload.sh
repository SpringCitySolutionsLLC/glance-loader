#!/bin/bash
#
# glance-loader/cirros/0.4.0/upload.sh
#

source /etc/kolla/admin-openrc.sh

openstack image create \
  --container-format bare \
  --public \
  --property os_type=linux \
  --property os_distro=cirros \
  --property os-version=0.4.0 \
  --property os_admin_user=cirros \
  --min-ram 256 \
  --min-disk 1 \
  --disk-format qcow2 \
  --file cirros-0.4.0-x86_64-disk.img \
  cirros-0.4.0

exit 0
