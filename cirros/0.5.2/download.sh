#!/bin/bash
#
# glance-loader/cirros/0.5.2/download.sh
#

wget -c http://download.cirros-cloud.net/0.5.2/cirros-0.5.2-x86_64-disk.img

echo md5sum value comes from
echo http://download.cirros-cloud.net/0.5.2/MD5SUMS 
echo
echo md5sum cirros-0.5.2-x86_64-disk.img result should be:
echo b874c39491a2377b8490f5f1e89761a4  cirros-0.5.2-x86_64-disk.img
echo
echo md5sum cirros-0.5.2-x86_64-disk.img is:
md5sum cirros-0.5.2-x86_64-disk.img

exit 0
