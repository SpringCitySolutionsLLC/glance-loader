#!/bin/bash
#
# glance-loader/fedora-cloud-base/36/upload.sh
#

source /etc/kolla/admin-openrc.sh

openstack image create \
  --container-format bare \
  --public \
  --property os_type=linux \
  --property os_distro=fedora \
  --property os-version=36 \
  --min-ram 1024 \
  --min-disk 1 \
  --disk-format qcow2 \
  --file Fedora-Cloud-Base-36-1.5.x86_64.qcow2 \
  fedora-cloud-base-36

exit 0
