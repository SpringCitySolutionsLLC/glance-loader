#!/bin/bash
#
# glance-loader/fedora-cloud-base/36/download.sh
#

wget -c https://download.fedoraproject.org/pub/fedora/linux/releases/36/Cloud/x86_64/images/Fedora-Cloud-Base-36-1.5.x86_64.qcow2

echo sha256sum value comes from
echo https://alt.fedoraproject.org/en/verify.html
echo
echo sha256sum Fedora-Cloud-Base-36-1.5.x86_64.qcow2 result should be:
echo ca9e514cc2f4a7a0188e7c68af60eb4e573d2e6850cc65b464697223f46b4605  Fedora-Cloud-Base-36-1.5.x86_64.qcow2
echo
echo sha256sum Fedora-Cloud-Base-36-1.5.x86_64.qcow2
sha256sum Fedora-Cloud-Base-36-1.5.x86_64.qcow2

exit 0
